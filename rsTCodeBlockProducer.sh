#!/bin/bash

# Example of use:
# ./$0 bash file.sh
# ./$0 python file.py



echo ".. code-block:: $1 "; echo; cat $2 | sed 's/^/   /'
