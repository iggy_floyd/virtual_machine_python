#!/usr/bin/env python

''' This is an example of the application of the Logistic fucntion to the dataset analysis. 
    Logistic function is helpful to model binary models

    This is a analysis with dedicated sklearn sub-modules.

    The basic idea is taken from 
    http://scikit-learn.org/stable/auto_examples/linear_model/plot_iris_logistic.html#example-linear-model-plot-iris-logistic-py
    http://scikit-learn.org/stable/auto_examples/grid_search_digits.html	
    http://www.pyimagesearch.com/2014/06/23/applying-deep-learning-rbm-mnist-using-python/

    DataFrameImputer was taken from 
    http://stackoverflow.com/questions/25239958/impute-categorical-missing-values-in-scikit-learn

    About Precision and Recall
    http://en.wikipedia.org/wiki/Precision_and_recall

    About Type 1 and Type 2 errors 
    http://en.wikipedia.org/wiki/Type_I_and_type_II_errors

    About scorring parameters for sklearn classifiers
    http://scikit-learn.org/stable/modules/model_evaluation.html#scoring-parameter

    About plottig ROC in sklearn
    http://scikit-learn.org/stable/auto_examples/plot_roc_crossval.html

    About plottig Precision vs Recall in sklearn
    http://scikit-learn.org/stable/auto_examples/plot_precision_recall.html

'''

import sys
sys.path = ['/usr/local/lib/python2.7/dist-packages'] + sys.path # to fix the problem with numpy: this replaces  1.6 version by 1.9
import pandas as pd
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.cross_validation import cross_val_score
from sklearn.preprocessing import Imputer
from sklearn import preprocessing
import numpy as np
from sklearn.base import TransformerMixin
from StringIO import StringIO
import prettytable    
import statsmodels.api as sm
import pylab as pl 
from sklearn import linear_model
from sklearn.cross_validation import train_test_split
from sklearn.grid_search import GridSearchCV
from sklearn.metrics import classification_report
from sklearn.metrics import roc_curve, auc,precision_recall_curve

class DataFrameImputer(TransformerMixin):

    def __init__(self):
        """Impute missing values.

        Columns of dtype object are imputed with the most frequent value 
        in column.

        Columns of other types are imputed with mean of column.

        """
    def fit(self, X, y=None):

        self.fill = pd.Series([X[c].value_counts().index[0]
            if X[c].dtype == np.dtype('O') else X[c].mean() for c in X],
            index=X.columns)

        return self

    def transform(self, X, y=None):
        return X.fillna(self.fill)



def transform_data(train_df,input_variable_names):
 ''' transform categorial data in train_df to numerical'''


 input_variable_types = map(lambda x: 'string' if 'object' in train_df[x].ftypes else train_df[x].ftypes,input_variable_names )
 labelencoders = []


 for i,x in enumerate(input_variable_types):
## here transform string label to numeric labels
  if 'string' in x:
   values =  map(lambda x: x if x!=np.nan else 'NaN' ,train_df[input_variable_names[i]].get_values())
   labelencoders+=[preprocessing.LabelEncoder()]   
   labelencoders[-1].fit(values)
   train_df[input_variable_names[i]] =  train_df[input_variable_names[i]].apply(lambda x: labelencoders[-1].transform(x))

 return labelencoders 


# 1) get data for an analysis
df = pd.read_csv("train.csv")

# 2) take a look at the dataset
print '**'*60,'\n'
print df.head()
print '**'*60,'\n'

# 3) summarize data
print '**'*60,'\n'
print df.describe()
print '**'*60,'\n'

# 6) we are going to analyze a regressiong factors between 'Survied' as a target and Pclass,Sex,Age,Fare,Embarked
# also we introduce dummy categories for all of the Pclass,Sex,Embarked  to do this analysis.
input_variable_names = ['Pclass','Sex','Embarked','Age','Fare']

# we need to understand the type of variables
input_variable_types = map(lambda x: 'string' if 'object' in df[x].ftypes else df[x].ftypes,input_variable_names )

# do variable transformation
encoders = transform_data(df,input_variable_names)

# apply  DataFrameImputer to fix np.nan
df = DataFrameImputer().fit(df).transform(df)

cols_to_keep = ['Survived','Pclass','Sex','Embarked','Age','Fare']
data = df[cols_to_keep]


print '**'*60,'\n'
print data.head()
print '**'*60,'\n'

# 7) split the sample for train and test sub-samples
# Split the dataset in two equal parts
train_cols = ['Pclass','Sex','Embarked','Age','Fare']
X_train, X_test, y_train, y_test = train_test_split(data[train_cols],data['Survived'] , test_size=0.5, random_state=0)

# Set the parameters by cross-validation and scores
tuned_parameters = {'C': [1, 10, 100, 10000,1e5,1e6]}
# the whole list of scoring types can be found here:
# http://scikit-learn.org/stable/modules/model_evaluation.html#scoring-parameter

# choose the best 'recall' strategy and start the grid
scoring = 'recall'
clf = GridSearchCV(linear_model.LogisticRegression(C=1e5), tuned_parameters, n_jobs = -1,verbose=1 ,cv=5, scoring=scoring)
clf.fit(X_train, y_train)
print("Best parameters:")
bestParams = clf.best_estimator_.get_params()
for p in sorted(tuned_parameters.keys()):
  print "\t %s: %f" % (p, bestParams[p])

print("Detailed classification report:")
print()
print("The model is trained on the full development set.")
print("The scores are computed on the full evaluation set.")
print()
y_true, y_pred = y_test, clf.best_estimator_.predict(X_test)
print(classification_report(y_true, y_pred))
print()


# 9) Compute ROC curve and area under the curve
probas_ = clf.best_estimator_.predict_proba(X_test)
fpr, tpr, thresholds = roc_curve(y_test, probas_[:, 1])
roc_auc = auc(fpr, tpr)
pl.plot(fpr, tpr, lw=1, label='ROC (area = %0.2f)' % (roc_auc))
pl.plot([0, 1], [0, 1], '--', color=(0.6, 0.6, 0.6), label='Luck')
pl.xlim([-0.05, 1.05])
pl.ylim([-0.05, 1.05])
pl.xlabel('False Positive Rate (FPR or Type-1 error )')
pl.ylabel('True Positive Rate (TPR or recall or Type-2 error)')
pl.title('Receiver operating characteristic example')
pl.legend(loc="lower right")
pl.show()

# 10) Compute Precision (PositivePredictiveValue=TP/(TP+FP))  vs recall (TPR=P/(TP+FN))
# http://scikit-learn.org/stable/auto_examples/plot_precision_recall.html
ppv, tpr, thresholds = precision_recall_curve(y_test, probas_[:, 1])
roc_auc = auc(tpr, ppv)
pl.plot(tpr, ppv, lw=1, label='Precsion-Recall curve (area = %0.2f)' % (roc_auc))
pl.xlim([-0.05, 1.05])
pl.ylim([-0.05, 1.05])
pl.ylabel('PPV (or precision)')
pl.xlabel('True Positive Rate (TPR or recall)')
pl.title('Precsion-Recall Curve example')
pl.legend(loc="lower right")
pl.show()


raw_input("Press the Enter key...")






