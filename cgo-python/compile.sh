#!/bin/bash
  program=`basename \`pwd\``

 INCDIR=`python -c "from distutils import sysconfig; print(sysconfig.get_python_inc())"`
 PLATINCDIR=`python -c "from distutils import sysconfig; print(sysconfig.get_python_inc(plat_specific=True))"`


  # compile c++ code into a static library
  for i in src/*.c; do g++ -fPIC -std=c++0x  -g -Iinclude -I$INCDIR -I$PLATINCDIR  -c ${i} ; done
  ar cru lib${program}.a *.o
  ranlib lib${program}.a
  [ ! -d lib ] && mkdir lib
  mv  lib${program}.a lib
  rm *.o 

  for i in src/*.c; do g++ -fPIC  -g -Iinclude -std=c++0x -I$INCDIR -I$PLATINCDIR  -c ${i}; done
  gcc -shared -o lib${program}.so  `ls *.o`
  mv  lib${program}.so lib
  rm *.o
