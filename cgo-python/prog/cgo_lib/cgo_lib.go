package cgo_lib

// #include "interface.h"
import "C"
import "unsafe"

func GoMain(Args []string) int {
	//var strPointer = new(*C.char)
	//*strPointer = C.CString("./go-cpp")
	//return int(C.CMain(1, strPointer))
	outer := make([]*C.char, len(Args)+1)
	for i, inner := range Args {
		outer[i] = C.CString(inner)
	}
	//	return int(C.CMain(C.int(len(Args)), Args))
	return int(C.CMain(C.int(len(Args)), (**C.char)(unsafe.Pointer(&outer[0]))))
}
