#!/bin/bash

mkdir -p package_$1/.libs
echo "dependences of the script... "
cp  $1 package_$1/.libs
./statifier.py $1  package_$1/.libs

# common modules
echo "copying common modules..."
mkdir -p package_$1/.libs/lib/
cp -r /usr/lib/python2.7/  package_$1/.libs/lib/



echo "writting the executable script..."
cat  > package_$1/$1 <<_EOF
#!/bin/bash

_abs_path_to_this_file=\$(readlink -f "\$0")
_local_dir=\$(dirname "\$_abs_path_to_this_file")
_libs_dir="\${_local_dir}/.libs"

export LD_LIBRARY_PATH=\${_libs_dir}:\$LD_LIBRARY_PATH
export PYTHONPATH=\${_libs_dir}/lib/python2.7/dist-packages/:\${_libs_dir}/lib/python2.7/:\${_libs_dir}/local/lib/python2.7/dist-packages/:\${_libs_dir}/local/lib/python2.7/:\$PYTHONPATH
export PYTHONHOME=\${_libs_dir}


.libs/$1 \$@

_EOF
chmod a+x package_$1/$1


echo "archiving..."
tar -zcf package_$1.tgz package_$1 
