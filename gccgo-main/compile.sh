#!/bin/bash
  program=`basename \`pwd\``

  INCDIR=`python -c "from distutils import sysconfig; print(sysconfig.get_python_inc())"`
  PLATINCDIR=`python -c "from distutils import sysconfig; print(sysconfig.get_python_inc(plat_specific=True))"`

  for i in prog/*.c; do gcc -fPIC  -g -Iinclude -std=c++0x -I$INCDIR -I$PLATINCDIR  -c ${i}; done
  for i	in prog/*.go; do gccgo   -c ${i}; done
