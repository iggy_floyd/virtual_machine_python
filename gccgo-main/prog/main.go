// gccgo-python is a simple package illustrating basic features of the binding python and go codes.
//
//
// You can clone the git repo
//
//     git clone https://iggy_floyd@bitbucket.org/iggy_floyd/cgo-cpp.git
//
// Afterwards, compile and execute
//
//    make
//    prog/prog-static
//
//
//
// The package also demonstrates a simple 'configuration' framework which is based the 'configure.ac' autoconf-script.
// One can add any wanted m4 macro to the 'configure.ac' and put any macro files to m4/ to adopt the 'configuration' checks for own
// purpose. Then simply run
//
//    autoreconf -ifv -I m4/
//
// to set up the  'configuration' framework of the package.
//
// Here is a list of open publications used to prepare this package:
//
//  https://cxwangyi.wordpress.com/2012/12/04/calling-c-from-go-without-using-swig/
//  https://github.com/wangkuiyi/go-cpp
//
//
package main
 
import "example"
import  "os"


func main()  int {
	var Args = os.Args
	var strPointer = new(string)
	 for _, inner := range Args {
	  *strPointer+= inner
             }	
	return 1
	return int(example.Main(len(Args), strPointer))
}


