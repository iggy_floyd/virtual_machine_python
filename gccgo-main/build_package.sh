#!/bin/bash

mkdir -p package_$1/.libs
echo "dependences of the script... "
cp  $1 package_$1/.libs
./statifier.py $1  package_$1/.libs

# common modules
echo "copying common modules..."
mkdir -p package_$1/.libs/lib/
cp -r /usr/lib/python2.7/  package_$1/.libs/lib/

# scientific python modules and other stuff
echo "copying scientific python modules and other stuff..."
mkdir -p package_$1/.libs/local/lib/
cp -r /usr/local/lib/python2.7/  package_$1/.libs/local/lib/


# numpy hack!
# add numerical support for numpy!
echo "adding numerical support for numpy!..."
cp /usr/lib/libblas.so.3 package_$1/.libs/
./statifier.py /usr/lib/libblas.so.3 package_$1/.libs/
cp /usr/lib/liblapack.so.3  package_$1/.libs/


# a hack to add hashlib support
echo "hacking to add hashlib support..."
ls /usr/lib/python2.7/lib-dynload/  | xargs -I {} ./statifier.py /usr/lib/python2.7/lib-dynload/{} package_$1/.libs/

# a hack to add matlab support
echo "hacking to add matplotlib support..."
ls /usr/lib/python2.7/dist-packages/gi/  | grep .so | xargs -I {} ./statifier.py /usr/lib/python2.7/dist-packages/gi/{} package_$1/.libs/
#cp -r /usr/share/pyshared/gi/   /usr/lib/python2.7/dist-packages/
rsync /usr/share/pyshared/gi package_$1/.libs/lib/python2.7/dist-packages/  -a --copy-links -v
rsync /usr/share/pyshared/wx-2.8-gtk2-unicode/wx package_$1/.libs/lib/python2.7/dist-packages/wx-2.8-gtk2-unicode/  -a --copy-links -v
rsync /usr/share/pyshared/wx-2.8-gtk2-unicode/wxPython package_$1/.libs/lib/python2.7/dist-packages/wx-2.8-gtk2-unicode/  -a --copy-links -v

echo "hacking Tornado Service of the WebAgg..."
sed -i "706s/\(.*\)/#\1/"   package_$1/.libs/local/lib/python2.7/dist-packages/tornado/ioloop.py
sed -i "707s/\(.*\)/#\1/"   package_$1/.libs/local/lib/python2.7/dist-packages/tornado/ioloop.py



echo "writting the executable script..."
cat  > package_$1/$1 <<_EOF
#!/bin/bash

_abs_path_to_this_file=\$(readlink -f "\$0")
_local_dir=\$(dirname "\$_abs_path_to_this_file")
_libs_dir="\${_local_dir}/.libs"

export LD_LIBRARY_PATH=\${_libs_dir}:\$LD_LIBRARY_PATH
export PYTHONPATH=\${_libs_dir}/lib/python2.7/dist-packages/:\${_libs_dir}/lib/python2.7/:\${_libs_dir}/local/lib/python2.7/dist-packages/:\${_libs_dir}/local/lib/python2.7/:\$PYTHONPATH
export PYTHONHOME=\${_libs_dir}


.libs/$1 \$@

_EOF
chmod a+x package_$1/$1



# copy train.csv
cp train.csv package_$1
echo "archiving..."
tar -zcf package_$1.tgz package_$1 
