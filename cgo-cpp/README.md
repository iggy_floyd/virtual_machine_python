# prog
--
This is the documentation of the cgo-cpp


### Introduction

cgo-cpp is a simple package illustrating basic features of the binding c++ and
go codes.

To view this godoc on your own machine, just run

    go get code.google.com/p/go.tools/cmd/godoc
    go get -u bitbucket.org/iggy_floyd/cgo_cpp  #(this repo is made to be public, to allow such "go-getting" )
    godoc -http=:8080

Then you can open documentation in the browser:

    http://localhost:8080/pkg/bitbucket.org/iggy_floyd/go-cpp/

You can clone the git repo

    git clone https://iggy_floyd@bitbucket.org/iggy_floyd/cgo-cpp.git

Afterwards, compile and execute

    make
    prog/prog-static

The package also demonstrates a simple 'configuration' framework which is based
the 'configure.ac' autoconf-script. One can add any wanted m4 macro to the
'configure.ac' and put any macro files to m4/ to adopt the 'configuration'
checks for own purpose. Then simply run

    autoreconf -ifv -I m4/

to set up the 'configuration' framework of the package.

Here is a list of open publications used to prepare this package:

    https://cxwangyi.wordpress.com/2012/12/04/calling-c-from-go-without-using-swig/
    https://github.com/wangkuiyi/go-cpp

If it complains cannot find the shared library when you run this program, please
export DYLD_LIBRARY_PATH=<go-cpp/c++> if you use OS X, or export
LD_LIBRARY_PATH=<go-cpp/c++> if you use Linux.
