#include "work.h"
// Thie file, as a C++ source file, should not include C interfacing
// header files, i.e., "interface.h".


extern "C" {
#include <stdio.h>
  long CRandom() {
    // As the rng is seeded in every call, it is not random at all.
    RandomNumberGenerator r(1);
    printf ("Characters: %c %c \n", 'a', 65);
    printf ("Decimals: %d %ld\n", 1977, 650000L);
    printf ("Preceding with blanks: %10d \n", 1977);
    printf ("Preceding with zeros: %010d \n", 1977);
    printf ("Some different radices: %d %x %o %#x %#o \n", 100, 100, 100, 100, 100);
    printf ("floats: %4.2f %+.0e %E \n", 3.1416, 3.1416, 3.1416);
    printf ("Width trick: %*d \n", 5, 10);
    printf ("%s \n", "A string");
    return r.Rand();
  }
}
