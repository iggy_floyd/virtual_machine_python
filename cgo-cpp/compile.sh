#!/bin/bash
  program=`basename \`pwd\``

  # compile c++ code into a static library
  for i in src/*.cc; do g++ -fPIC -std=c++0x  -g -Iinclude  -c ${i}; done
  for i in src/*.c; do g++ -fPIC -std=c++0x  -g -Iinclude  -c ${i}; done
  ar cru lib${program}.a *.o
  ranlib lib${program}.a
  [ ! -d lib ] && mkdir lib
  mv  lib${program}.a lib
  rm *.o 

  #compile C wrapper of the C++ code
  #for i in src/*.c; do g++ -fPIC  -g -Iinclude  -c ${i}; done
  #gcc -shared -o lib${program}.so  `ls *.o`
  #mv  lib${program}.so lib
  #rm *.o
