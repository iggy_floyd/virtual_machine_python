#!/bin/bash
  program=`basename \`pwd\``
  
  echo `pwd`
  cat /tmp/.arch

  # compile c++ code into a static library
  for i in src/*.cc; do echo g++ -fPIC -std=c++0x `cat /tmp/.arch`  -g -Iinclude  -c ${i}; done
  for i in src/*.c; do echo g++ -fPIC -std=c++0x `cat  /tmp/.arch` -g -Iinclude  -c ${i}; done
  for i in src/*.cc; do  g++ -fPIC -std=c++0x `cat /tmp/.arch`  -g -Iinclude  -c ${i}; done
  for i in src/*.c; do  g++ -fPIC -std=c++0x `cat  /tmp/.arch` -g -Iinclude  -c ${i}; done
  ar cru lib${program}.a *.o
  ranlib lib${program}.a
  [ ! -d lib ] && mkdir lib
  mv  lib${program}.a lib
  rm *.o 

  #compile C wrapper of the C++ code
  for i in src/*.c; do g++ -fPIC `cat /tmp/.arch`  -g -Iinclude  -c ${i}; done
  for i in src/*.c; do echo g++ -fPIC `cat /tmp/.arch`  -g -Iinclude  -c ${i}; done
  gcc -shared -o lib${program}.so  `cat /tmp/.arch`  `ls *.o`
  echo gcc -shared -o lib${program}.so  `cat /tmp/.arch`  `ls *.o`
  mv  lib${program}.so lib
  rm *.o
