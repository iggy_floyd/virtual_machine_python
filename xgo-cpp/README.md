# xgo-cpp

This is a simple project supporting the example of the `xgo` [cross-compilation](https://github.com/karalabe/xgo/) given in 
the tutorial [**Statifiers for Python and C++. Statifiering python Machine-Learning scripts**](https://bitbucket.org/iggy_floyd/virtual_machine_python).

