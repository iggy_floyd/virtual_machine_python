# gccgo-python
--
@ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de


### Introduction

gccgo-python is a simple package illustrating basic features of the binding
python and go codes.

You can clone the git repo

    git clone https://iggy_floyd@bitbucket.org/iggy_floyd/virtual_machine_python.git

Afterwards, compile and execute the binary

    cd gccgo-python
    make
    gccgo-python

Then you can open documentation in the browser:

    go get code.google.com/p/go.tools/cmd/godoc
    go get -u bitbucket.org/iggy_floyd/virtual_machine_python/gccgo-python  #(this repo is made to be public, to allow such "go-getting" )
    godoc -http=:8080


### References

Here is a list of open publications used to prepare this package:

    https://cxwangyi.wordpress.com/2012/12/04/calling-c-from-go-without-using-swig/
    https://github.com/wangkuiyi/go-cpp
