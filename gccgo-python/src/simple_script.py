'''
https://docs.python.org/2.7/library/argparse.html
'''

import argparse

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('integers', metavar='N', type=int, nargs='+',
                   help='an integer for the accumulator')
parser.add_argument('--sum', dest='accumulate', action='store_const',
                   const=sum, default=max,
                   help='sum the integers (default: find the max)')


if __name__ == "__main__":
 args = parser.parse_args()
 print args.accumulate(args.integers)


