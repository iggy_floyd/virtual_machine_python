
#!/bin/bash

mkdir -p package_$1/.libs
echo "dependences of the script... "
cp  $1 package_$1/.libs
./statifier.py $1  package_$1/.libs
chmod a+x `ls package_$1/.libs/ld-linux* | tail -1`


echo "writting the executable script..."
cat  > package_$1/$1 <<_EOF
#!/bin/bash

_abs_path_to_this_file=\$(readlink -f "\$0")
_local_dir=\$(dirname "\$_abs_path_to_this_file")
_libs_dir="\${_local_dir}/.libs"

export LD_LIBRARY_PATH=\${_libs_dir}:\$LD_LIBRARY_PATH
#_ld_loader=\`ls .libs/ld-linux*so* | tail -1\`
#\${_ld_loader}  --library-path  .libs/ .libs/$1 \$@

# http://pyinstaller.narkive.com/msP06AuN/problem-with-different-versions-of-linux
find .libs/ -iname "*libc*so*" -exec rm {} \;

.libs/$1 \$@

_EOF
chmod a+x package_$1/$1


echo "archiving..."
tar -zcf package_$1.tgz package_$1 
