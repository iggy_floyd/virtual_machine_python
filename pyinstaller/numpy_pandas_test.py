#!/usr/bin/env python


# Main Program starts here
def main():
 import sys
 sys.path = ['/usr/local/lib/python2.7/dist-packages'] + sys.path # to fix the problem with numpy: this replaces  1.6 version by 1.9
 
 print sys.path
 import numpy as np
 # it doesn't work because of the incompatibility with numpy
 #import pandas as pd

 # 1) print a numpy array
 print np.array([1,2,3,4])

 # 2) take a look at the dataset
 #df = pd.read_csv("train.csv")
 #print '**'*60,'\n'
 #print df.head()
 #print '**'*60,'\n'

main()

